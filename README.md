# Watchmake

A configurable version of `$ watch make`. `watchmake --help` yields:

```
usage: watchmake [-h] [-e EXTENSIONS] [-q] [-i IGNORE] [-m MAKE_CMD]
                 [files ...]

Watch files and make on changes

positional arguments:
  files                 Files or directories to watch. Defaults to current
                        directory

options:
  -h, --help            show this help message and exit
  -e EXTENSIONS, --ext EXTENSIONS
                        Only watch files with this extension
  -q, --quiet           Run quietly
  -i IGNORE, --ignore IGNORE
                        Ignore files or folders. Use a leading "/" to denote
                        files at the top level. Globs not supported
  -m MAKE_CMD, --make-cmd MAKE_CMD
                        Define command to run upon modification of files.
                        Defaults to `make`. You may use {} in the command and
                        it will be replace by the changed file
```
