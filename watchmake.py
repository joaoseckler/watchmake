#!/usr/bin/env python3

import argparse
import signal
import sys
import subprocess as sp
import shutil
from pathlib import Path


def split_ignore(options):
    setattr(
        options,
        "root_ignore",
        list(map(
            lambda f: Path(f[1:]).resolve(),
            filter(lambda f: f[0] == "/", options.ignore)
        ))

    )

    options.ignore = list(filter(lambda f: f[0] != "/", options.ignore))

def ext_add_dots(options):
    if options.extensions:
        options.extensions = list(map(
            lambda e: "." + e if e[0] != "." else e,
            options.extensions
        ))

def parse():
    parser = argparse.ArgumentParser(
        prog="watchmake",
        description="Watch files and make on changes"
    )

    parser.add_argument(
        "files",
        nargs="*",
        default=".",
        help="Files or directories to watch. Defaults to current directory",
    )

    parser.add_argument(
        "-e",
        "--ext",
        dest="extensions",
        action="append",
        help="Only watch files with this extension",
    )

    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="Run quietly",
    )

    parser.add_argument(
        "-i",
        "--ignore",
        action="append",
        default=[],
        help=(
            "Ignore files or folders. Use a leading \"/\" to denote "
            "files at the top level. Globs not supported"
        )
    )

    parser.add_argument(
        "-m",
        "--make-cmd",
        default="make",
        help=(
            "Define command to run upon modification of files. Defaults "
            "to `make`. You may use {} in the command and it will be "
            "replace by the changed file"
        ),
    )

    options = parser.parse_args()
    split_ignore(options)
    ext_add_dots(options)

    return options

def handle_sigint(sig, frame):
    print("\n\n", end="")
    print("Who watches the watchmen? Bye.")
    sys.exit(0)

def print_line(cols):
    print("-" * (cols - 4))

def print_files(files):
    length = sum(map(len, files)) + 2 * (len(files) - 1)
    cols = shutil.get_terminal_size((80, 20)).columns

    print_line(cols)

    if length > cols:
        print(
            "Waiting for:\n",
            "\n".join(f"  * {file}" for file in files),
            sep=""
        )
    else:
        print("Waiting for", ", ".join(files))

def folder_to_file_list(file):
    if Path(file).is_file():
        return [Path(file)]

    return list(filter(Path.is_file, Path(file).rglob("*")))

def watched_files(options):
    all_files = sum(map(folder_to_file_list, options.files), [])
    return list(
        map(
            str,
            filter(
                lambda f: (
                    (f.is_file() or f.is_dir())
                    and (f.suffix in options.extensions if options.extensions else True)
                    and all(part not in options.ignore for part in f.parts)
                    and all(
                        Path(i).resolve() not in f.resolve().parents
                        for i in options.root_ignore
                    )
                ),
            all_files
            )
        )
    )



def watch(files, options):
    if not options.quiet:
        print_files(files)

    cmd = [
        *"inotifywait --format '%w' -e modify -e move_self".split(),
        *files
    ]
    (stdout, stderr)  = sp.Popen(
        cmd,
        stdout=sp.PIPE,
        stderr=sp.PIPE,
    ).communicate()


    if not options.quiet:
        print("Compiling...")

    changed = stdout.decode("utf-8")
    sp.run(options.make_cmd.replace("{}", changed), shell=True, close_fds=False)

def main(options):
    signal.signal(signal.SIGINT, handle_sigint)

    while True:
        files = watched_files(options)

        if not files:
            print("No files found! Exiting...")
            sys.exit(1)

        watch(files, options)


if __name__ == "__main__":
    main(parse())
